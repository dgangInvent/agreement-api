using Microsoft.EntityFrameworkCore;
using Agreement.API.Models;

namespace Agreement.API.Models.Contexts
{
    public class AppINOWDBContext : DbContext
    {
        public DbSet<Camp_Data_SiteCode> Camp_Data_SiteCode { get; set; }
        public DbSet<Camp_Data> Camp_Data { get; set; }
        public DbSet<Camp_Data_Positions> Camp_Data_Positions { get; set; }
        public DbSet<position> position { get; set; }
        public DbSet<Verbiage> Verbiage { get; set; }
        public DbSet<verbiageConfiguration> verbiageConfiguration { get; set; }
        public AppINOWDBContext(DbContextOptions<AppINOWDBContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Camp_Data>(entity =>
            {
                entity.HasKey(e => e.id);
            });
            modelBuilder.Entity<Camp_Data>().Property(p => p.firstname).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.lastname).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.address1).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.email).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.ProgramTypeID).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.dayphone).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.zip).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.state).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.currentSchool).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.currentDistrict).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.city).IsRequired();
            modelBuilder.Entity<Camp_Data>().Property(p => p.begindate).IsRequired().HasColumnType("datetime2");
            modelBuilder.Entity<Camp_Data>().Property(p => p.enddate).IsRequired().HasColumnType("datetime2");

            modelBuilder.Entity<Camp_Data_SiteCode>(entity =>
            {
                entity.HasKey(e => e.SiteCodeID);
            });
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.SiteName).IsRequired();
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.SiteStatus).IsRequired();
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.StartDate).IsRequired();
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.State).IsRequired();
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.Program).IsRequired();
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.EndDate).IsRequired();
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.City).IsRequired();
            modelBuilder.Entity<Camp_Data_SiteCode>().Property(p => p.CampCode).IsRequired();

            modelBuilder.Entity<Camp_Data_Positions>(entity =>
            {
                entity.HasKey(e => e.PosID);
            });

            modelBuilder.Entity<position>(entity =>
            {
                entity.HasKey(e => e.id);
            });

            modelBuilder.Entity<Verbiage>(entity =>
            {
                entity.HasKey(e => e.id);
            });

            modelBuilder.Entity<verbiageConfiguration>(entity =>
            {
                entity.HasKey(e => e.id);
            });
        }
    }
}

