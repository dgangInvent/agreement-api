namespace Agreement.API.Models
{
    public class Camp_Data_Positions
    {
        public int PosID { get; set; }
        public string PositionName { get; set; }
        public int PositionID { get; set; }
        public string CampCode { get; set; }
    }
}

