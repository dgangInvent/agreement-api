

using System;

namespace Agreement.API.Models
{
    public class UserAgreement
    {
        public string programType { get; set; }
        public string programState { get; set; }
        public string programCity { get; set; }
        public string programLocation { get; set; }
        public string position { get; set; }
        public string programStartDate { get; set; }
        public string programEndDate { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string homeAddress { get; set; }
        public string homeAddressAddon { get; set; }
        public string userCity { get; set; }
        public string userState { get; set; }
        public string userZip { get; set; }
        public string userZipAddon { get; set; }
        public string cell { get; set; }
        public string altPhone { get; set; }
        public bool textAlerts { get; set; }
        public string email { get; set; }
        public string preferredMethodofContact { get; set; }
        public string secondaryMethodofContact { get; set; }
        public string currentSchool { get; set; }
        public string currentSchoolDistrict { get; set; }
        public string dateofBirth { get; set; }
        public bool stipendDirect { get; set; }
        public bool volunteer { get; set; }
        public bool stipendthirdParty { get; set; }
        public bool previouslyServed { get; set; }
        public bool isPaidPos { get; set; }
        public int soc { get; set; }
    }
}