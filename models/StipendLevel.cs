using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Agreement.API.Models
{
    public class stipendlevel
    {
        public Guid  id { get; set; }
        public int ParticipantCountMin { get; set; }
        public int ParticipantCountMax { get; set; }
        public decimal StipendAmountNew { get; set; }
        public decimal StipendAmountReturning { get; set; }
        public Guid  RoleId { get; set; }
        public int PricingTypeId { get; set; }
    }
    public class Stipend
    {
        public int ParticipantCountMin { get; set; }
        public int ParticipantCountMax { get; set; }
        public decimal StipendAmountNew { get; set; }
        public decimal StipendAmountReturning { get; set; }
        public string RoleName { get; set; }//not a db feild
        public string PricingTypeName { get; set; }//not a db feild
    }
}