namespace Agreement.API.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    public class Camp_Data
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string campcode { get; set; }
        public string sitename { get; set; }
        [Column(TypeName = "Date")]
        public DateTime begindate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime enddate { get; set; }
        public string returnstaff { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string dayphone { get; set; }
        public string altphone { get; set; }
        public string email { get; set; }
        public string socnum { get; set; }
        public string dob { get; set; }
        public string position { get; set; }
        public string signedagreement { get; set; }
        public string payapproval { get; set; }
        public string reason { get; set; }
        public string notes { get; set; }
        public DateTime timestamp { get; set; }
        public string txtpay { get; set; }
        public string txtpay4 { get; set; }
        public string txtpay3 { get; set; }
        public string txtpay2 { get; set; }
        public string txtparentname { get; set; }
        public string txtparentemail { get; set; }
        public string txtmodule6 { get; set; }
        public string txtmodule5 { get; set; }
        public string txtmodule4 { get; set; }
        public string txtmodule3 { get; set; }
        public string txtmodule2 { get; set; }
        public string txtmodule1 { get; set; }
        public decimal donationamt { get; set; }
        public bool pay_thirdparty { get; set; }
        public string pay_thirdparty_comments { get; set; }
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public System.Guid GUID { get; set; }
        public DateTime? dir_approval { get; set; }
        public DateTime? rc_approval { get; set; }
        public DateTime? oper_approval { get; set; }
        public DateTime? admin_approval { get; set; }
        public DateTime? mail_date { get; set; }
        public int ProgramTypeID { get; set; }
        public bool comp_receive { get; set; }
        public bool comp_scholar { get; set; }
        public string comp_scholar_school { get; set; }
        public bool comp_org { get; set; }
        public string comp_org_name { get; set; }
        public bool electronic_agreement { get; set; }
        public bool e_curriculum { get; set; }
        public string e_signature { get; set; }
        public int pricingtype { get; set; }
        public int agetargeted { get; set; }
        public int TotalParticipants { get; set; }
        public int TotalCIT { get; set; }
        public decimal VLIBonus { get; set; }
        public int isPhoneAlertEnabled { get; set; }
        public bool Active { get; set; }
        public string currentSchool { get; set; }
        public string currentDistrict { get; set; }
        public string primaryContact { get; set; }
        public string secondaryContact { get; set; }
        public string extendedShift { get; set; }
    }

    /*this is for the state drop down. 
    this obj will be converted to a json,
    it will happen in the CampDataService.cs GetStateAsync() */
    public class UsStates
    {
        public List<string> value { get; set; }
    }

    public class Citys
    {
        public List<string> value { get; set; }
    }

    public class Locations
    {
        public List<string> value { get; set; }
    }

    public class PositionAvailable
    {
        public List<string> value { get; set; }
    }

    public class DateRange
    {
        public List<string> value { get; set; }
    }
}
