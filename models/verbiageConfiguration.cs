namespace Agreement.API.Models
{
    public class verbiageConfiguration
    {
        public int id { get; set; }
        public int pricingTypeId { get; set; }
        public int positionId { get; set; }
        public int verbiageId { get; set; }
        public int groupId { get; set; }
        public int vorder { get; set; }
        public int extended { get; set; }
        public bool previouslyServed { get; set; }
        public bool legal { get; set; }
        public bool stipendInfo { get; set; }
        public bool funded { get; set; }
        public bool stipendThirdParty { get; set; }
        public bool stipendVolunteer { get; set; }
        public bool stipendDirect { get; set; }
        public bool ageTargeted { get; set; }

    }
}