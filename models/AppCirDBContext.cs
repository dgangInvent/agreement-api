using Microsoft.EntityFrameworkCore;
using Agreement.API.Models;
using MySqlConnector;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Agreement.API.Models.Contexts
{
    public class AppCirDBContext : DbContext
    {
        public DbSet<aspnetroles> aspnetroles { get; set; }

        public DbSet<stipendlevel> stipendlevel { get; set; }

        public DbSet<pricingtype> pricingtype { get; set; }

        public AppCirDBContext(DbContextOptions<AppCirDBContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<aspnetroles>(entity =>
            {
                entity.HasKey(e => e.id);
            });
            modelBuilder.Entity<aspnetroles>().Property(p => p.Name).IsRequired();
            modelBuilder.Entity<aspnetroles>().Property(p => p.RoleId).IsRequired();

            modelBuilder.Entity<stipendlevel>(entity =>
            {
                entity.HasKey(e => e.id);
            });
            modelBuilder.Entity<stipendlevel>().Property(p => p.ParticipantCountMin).IsRequired();
            modelBuilder.Entity<stipendlevel>().Property(p => p.ParticipantCountMax).IsRequired();
            modelBuilder.Entity<stipendlevel>().Property(p => p.StipendAmountNew).IsRequired();
            modelBuilder.Entity<stipendlevel>().Property(p => p.StipendAmountReturning).IsRequired();
            modelBuilder.Entity<stipendlevel>().Property(p => p.RoleId).IsRequired();

            modelBuilder.Entity<pricingtype>(entity =>
            {
                entity.HasKey(e => e.PricingTypeId);
            });
            modelBuilder.Entity<pricingtype>().Property(p => p.Name).IsRequired();
        }
    }
}

