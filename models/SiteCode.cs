namespace Agreement.API.Models
{
    using System;
    public class Camp_Data_SiteCode
    {
        public int SiteCodeID { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string CampCode { get; set; }
        public string SiteName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string RC_RPM_Email { get; set; }
        public string RC_RPM_Email2 { get; set; }
        public string Program { get; set; }
        public int ProgramTypeID { get; set; }
        public string ExtendedDay { get; set; }
        public string LI_Type { get; set; }
        public string SiteStatus { get; set; }
        public int PricingType { get; set; }
        public int AgeTargeted { get; set; }
        public string DirectorPin { get; set; }
        public string SessionModAmount { get; set; }
    }
}
