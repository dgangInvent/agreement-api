using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
namespace Agreement.API.Models
{
    [Table("verbiage")]
    public class Verbiage
    {
        public int id { get; set; }
        public string title { get; set; }
        public string verbiage { get; set; }
    }

    public class VerbTemp
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string programLocation { get; set; }
        public string programCity { get; set; }
        public string programState { get; set; }
        public string programStartDate { get; set; }
        public string programEndDate { get; set; }
        public string amtPaid { get; set; }
    }
}