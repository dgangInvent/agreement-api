using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Agreement.API.Models
{
    public class pricingtype
    {
        public int PricingTypeId { get; set; }
        public string Name { get; set; }
    }
}