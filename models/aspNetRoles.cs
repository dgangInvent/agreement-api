using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Agreement.API.Models
{
    public class aspnetroles
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
        public string Discriminator { get; set; }
    }
}