using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Agreement.API.Models.Contexts;
using Microsoft.EntityFrameworkCore;
using Agreement.API.Services.Interface;
using Agreement.API.Persistence.Repositories;
using Agreement.API.Persistence.Repositories.Interface;
using Agreement.API.Services;
using Newtonsoft.Json;
using MySqlConnector;

namespace Agreement.API
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        //public void ConfigureServices(IServiceCollection services)
        //{
        //    services.AddControllers();
        //}
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                                builder =>
                                {
                                    builder.WithOrigins("http://localhost:8080",
                                                        "https://localhost:5001")
                                                        .AllowAnyHeader()
                                                        .AllowAnyMethod();
                                });
            });

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Newtonsoft.Json.Formatting.None
                /*                 ,
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore */
            };

            services.AddDbContext<AppINOWDBContext>
                (opt => opt.UseSqlServer(Configuration["Data:CommandAPIConnection:ConnectionStringINOWDB"]));

            services.AddDbContext<AppCirDBContext>
                (opt => opt.UseMySql(Configuration["Data:CommandAPIConnection:ConnectionStringCir"]));

            services.AddControllers();
            services.AddScoped<IcampDataService, CampDataService>();
            services.AddScoped<IcampDataRepo, CampDataRepository>();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
