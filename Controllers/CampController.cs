using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Agreement.API.Models;
using Agreement.API.Services;
using Agreement.API.Services.Interface;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using System;

namespace Agreement.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class CampController : Controller
    {
        private readonly IcampDataService _campDataService;

        public CampController(IcampDataService campDataService)
        {
            _campDataService = campDataService;
        }

        [EnableCors]
        [HttpGet("getusstates/{position}")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetStates(string position)
        {
            var states = _campDataService.GetStatesAsync(position);
            return states.Result;
        }

        [EnableCors]
        [HttpGet("getcitys/{usstate}/{position}")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetCitys(string usstate, string position)
        {
            var citys = _campDataService.GetCityAsync(usstate, position);
            return citys.Result;
        }

        [EnableCors]
        [HttpGet("getlocations/{usstate}/{city}/{position}")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetLocations(string usstate, string city, string position)
        {
            var locas = _campDataService.GetLocationAsync(usstate, city, position);
            return locas.Result;
        }

        [EnableCors]
        [HttpGet("getPosition/{campCode}")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetPositions(string campCode)
        {
            var pos = _campDataService.GetPositionAsync(campCode);
            return pos.Result;
        }

        [EnableCors]
        [HttpGet("getDateRange/{campCode}")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetDateRange(string campCode)
        {
            var date = _campDataService.GetDateRangeAsync(campCode);
            return date.Result;
        }

        [EnableCors]
        [HttpGet("getstipend/{position}")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetStipend(string position)
        {
            var stipend = _campDataService.GetStipend(position);
            return stipend;
        }

        [EnableCors]
        [HttpGet("getstipend")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetStipend()
        {
            var stipend = _campDataService.GetStipend();
            return stipend;
        }

        [EnableCors]
        [HttpGet("getverbiage/{firstName}/{lastName}/{programLocation}/{programCity}/{programState}/{programStartDate}/{programEndDate}/{amtPaid}/{previouslyServed}/{position}/{PricingTypeName}/{isLegal}/{isStipendInfo}/{isVol}/{isThird}/{isDir}")]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<string> GetVerbiage(string firstName, string lastName, string programLocation, string programCity,
        string programState, string programStartDate, string programEndDate, string amtPaid, bool previouslyServed,
        string position, string PricingTypeName, bool isLegal, bool isStipendInfo, bool isVol, bool isThird, bool isDir)
        {
            var verb = _campDataService.GetVerbiage(firstName, lastName, programLocation, programCity,
                programState, programStartDate, programEndDate, amtPaid, previouslyServed,
                position, PricingTypeName, isLegal, isStipendInfo, isVol, isThird, isDir);
            return verb;
        }

        [EnableCors]
        [HttpPost("postuseragremment")]
        [Route("postuseragremment")]
        [Consumes("application/x-www-form-urlencoded")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 500)]
        public IActionResult PostUserAgremment([FromForm] IFormCollection uaform)
        {
            var userAgreement = new UserAgreement();

            userAgreement.firstName = uaform["firstName"];
            userAgreement.lastName = uaform["lastName"];
            userAgreement.position = uaform["position"];
            userAgreement.preferredMethodofContact = uaform["preferredMethodofContact"];
            userAgreement.previouslyServed = bool.Parse(uaform["previouslyServed"]);
            userAgreement.programCity = uaform["programCity"];
            userAgreement.programEndDate = uaform["programEndDate"];
            userAgreement.programStartDate = uaform["programStartDate"];
            userAgreement.programState = uaform["programState"];
            userAgreement.programType = uaform["programType"];
            userAgreement.secondaryMethodofContact = uaform["secondaryMethodofContact"];
            userAgreement.soc = uaform["soc"] != "" ? Int32.Parse(uaform["soc"]) : 0;
            userAgreement.stipendDirect = bool.Parse(uaform["stipendDirect"]);
            userAgreement.stipendthirdParty = bool.Parse(uaform["stipendthirdParty"]);
            userAgreement.textAlerts = bool.Parse(uaform["textAlerts"]);
            userAgreement.userCity = uaform["userCity"];
            userAgreement.userState = uaform["userState"];
            userAgreement.userZip = uaform["userZip"];
            userAgreement.userZipAddon = uaform["userZipAddon"];
            userAgreement.volunteer = bool.Parse(uaform["volunteer"]);
            userAgreement.altPhone = uaform["altPhone"];
            userAgreement.cell = uaform["cell"];
            userAgreement.currentSchool = uaform["currentSchool"];
            userAgreement.currentSchoolDistrict = uaform["currentSchoolDistrict"];
            userAgreement.dateofBirth = uaform["dateofBirth"];
            userAgreement.email = uaform["email"];
            userAgreement.homeAddress = uaform["homeAddress"];
            userAgreement.homeAddressAddon = uaform["homeAddressAddon"];
            userAgreement.isPaidPos = bool.Parse(uaform["isPaidPos"]);
            userAgreement.programLocation = uaform["programLocation"];
            _campDataService.PostAgreement(userAgreement);
            return Json("Agreement post ran");
        }
    }
}