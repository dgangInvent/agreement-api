using Agreement.API.Models.Contexts;

namespace Agreement.API.Persistence.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly AppINOWDBContext _inowdbContext;
        protected readonly AppCirDBContext _cirdbContext;

        public BaseRepository(AppINOWDBContext inowdbContext, AppCirDBContext cirdbContext)
        {
            _inowdbContext = inowdbContext;
            _cirdbContext = cirdbContext;
        }
    }
}