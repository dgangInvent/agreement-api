using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Agreement.API.Models;
using Agreement.API.Persistence.Repositories;
using Agreement.API.Models.Contexts;
using Agreement.API.Persistence.Repositories.Interface;
using System.Linq;
using System;
using Nustache.Core;

namespace Agreement.API.Persistence.Repositories
{
    public class CampDataRepository : BaseRepository, IcampDataRepo
    {
        public CampDataRepository(AppINOWDBContext inowdbContext, AppCirDBContext cirdbContext) : base(inowdbContext, cirdbContext) { }

        public async Task<List<string>> GetStatesAsync(string position)
        {
            var id = GetProgramTypeId(position);
            var stateList = await _inowdbContext.Camp_Data_SiteCode
                    .Where(t => t.ProgramTypeID == id)
                    .Select(t => t.State)
                    .Distinct()
                    .ToListAsync();
            return stateList;
        }

        public async Task<List<string>> GetCityAsync(string usstate, string position)
        {
            var id = GetProgramTypeId(position);
            var cityList = await _inowdbContext.Camp_Data_SiteCode
                .Where(x => x.State == usstate && x.ProgramTypeID == id)
                .Select(t => t.City)
                .Distinct()
                .ToListAsync();
            return cityList;
        }

        public async Task<List<Camp_Data>> GetLocationAsync(string usstate, string city, string position)
        {
            var id = GetProgramTypeId(position);
            var locList = await _inowdbContext.Camp_Data_SiteCode
                .Where(x => x.State == usstate &&
                    x.City == city &&
                    x.ProgramTypeID == id &&
                    x.ProgramTypeID == id)
                .Select(t => new Camp_Data
                {
                    sitename = t.SiteName,
                    campcode = t.CampCode
                })
                .Distinct()
                .ToListAsync();
            return locList;
        }

        public async Task<List<string>> GetPositionAsync(string campCode)
        {
            var posList = await _inowdbContext.Camp_Data_Positions
                .Where(x => x.CampCode == campCode)
                .Select(t => t.PositionName)
                .Distinct()
                .ToListAsync();
            return posList;
        }

        public async Task<List<Camp_Data_SiteCode>> GetDateRangeAsync(string campCode)
        {
            var dates = await _inowdbContext.Camp_Data_SiteCode
                .Where(x => x.CampCode == campCode)
                .Select(t => new Camp_Data_SiteCode
                {
                    StartDate = t.StartDate,
                    EndDate = t.EndDate
                })
                .Distinct()
                .ToListAsync();
            return dates;
        }

        public List<Stipend> GetStipend(string position)
        {
            var stipendQuery = from stipend in _cirdbContext.stipendlevel
                               join roles in _cirdbContext.aspnetroles
                               on stipend.RoleId equals roles.id
                               join pricingtype in _cirdbContext.pricingtype
                               on stipend.PricingTypeId equals pricingtype.PricingTypeId
                               where roles.Name == position
                               orderby stipend.ParticipantCountMax, pricingtype.PricingTypeId
                               select new
                               {
                                   stipend.ParticipantCountMin,
                                   stipend.ParticipantCountMax,
                                   stipend.StipendAmountNew,
                                   stipend.StipendAmountReturning,
                                   roleName = roles.Name,
                                   pricingtypeName = pricingtype.Name
                               };

            var stipendList = new List<Stipend>();
            foreach (var stipend in stipendQuery)
            {
                var stipendlevel = new Stipend
                {
                    ParticipantCountMin = stipend.ParticipantCountMin,
                    ParticipantCountMax = stipend.ParticipantCountMax,
                    StipendAmountNew = stipend.StipendAmountNew,
                    StipendAmountReturning = stipend.StipendAmountReturning,
                    RoleName = stipend.roleName,//not a db feild
                    PricingTypeName = stipend.pricingtypeName//not a db feild
                };
                stipendList.Add(stipendlevel);
            }

            return stipendList;
        }

        public List<Stipend> GetStipend()
        {
            var stipendQuery = _cirdbContext.stipendlevel.Join(_cirdbContext.aspnetroles,
            stipend => stipend.RoleId,
            roles => roles.id,
            (stipend, roles) => new { stipendlevel = stipend, aspnetroles = roles })
            .OrderBy(x => x.aspnetroles.Name).ThenBy(y => y.stipendlevel.ParticipantCountMax);

            var stipendList = new List<Stipend>();
            foreach (var stipend in stipendQuery)
            {
                var stipendlevel = new Stipend
                {
                    ParticipantCountMin = stipend.stipendlevel.ParticipantCountMin,
                    ParticipantCountMax = stipend.stipendlevel.ParticipantCountMax,
                    StipendAmountNew = stipend.stipendlevel.StipendAmountNew,
                    StipendAmountReturning = stipend.stipendlevel.StipendAmountReturning,
                    RoleName = stipend.aspnetroles.Name//not a db feild
                };
                stipendList.Add(stipendlevel);
            }

            return stipendList;
        }

        public List<string> GetVerbiage(string firstName, string lastName, string programLocation, string programCity,
        string programState, string programStartDate, string programEndDate, string amtPaid, bool previouslyServed,
        string position, string PricingTypeName, bool isLegal, bool isStipendInfo, bool isVol, bool isThird, bool isDir)
        {
            if (string.IsNullOrEmpty(position))
            {
                position = "Director";
            }
            if (string.IsNullOrEmpty(PricingTypeName))
            {
                PricingTypeName = "Standard";
            }
            var pt = _cirdbContext.pricingtype.FirstOrDefault(t => t.Name == PricingTypeName);
            var verbQuery = from verbiageConfiguration in _inowdbContext.verbiageConfiguration
                            join verbiage in _inowdbContext.Verbiage
                            on verbiageConfiguration.verbiageId equals verbiage.id
                            join positionTable in _inowdbContext.position
                            on verbiageConfiguration.positionId equals positionTable.id
                            where
                            (positionTable.positionName == position &&
                            verbiageConfiguration.legal == isLegal &&
                            verbiageConfiguration.stipendInfo == isStipendInfo &&
                            verbiageConfiguration.previouslyServed == previouslyServed &&
                            verbiageConfiguration.stipendThirdParty == false &&
                            verbiageConfiguration.stipendVolunteer == false &&
                            verbiageConfiguration.stipendDirect == false) ||
                            (positionTable.positionName == position &&
                            verbiageConfiguration.legal == isLegal &&
                            verbiageConfiguration.stipendInfo == isStipendInfo &&
                            verbiageConfiguration.previouslyServed == previouslyServed &&
                            verbiageConfiguration.stipendVolunteer == isVol &&
                            verbiageConfiguration.stipendThirdParty == isThird &&
                            verbiageConfiguration.stipendDirect == isDir)
                            orderby verbiageConfiguration.groupId, verbiageConfiguration.vorder
                            select new
                            {
                                verbiage.verbiage,
                                verbiageConfiguration.pricingTypeId
                            };

            var verbList = new List<string>();
            var verbTemp = new VerbTemp
            {
                firstName = firstName,
                lastName = lastName,
                programLocation = programLocation,
                programCity = programCity,
                programState = programState,
                programEndDate = programEndDate,
                programStartDate = programStartDate
            };

            foreach (var v in verbQuery)
            {
                if (v.pricingTypeId == pt.PricingTypeId)
                {
                    var html = Render.StringToString(v.verbiage, verbTemp);
                    verbList.Add(html);
                }
            }
            return verbList;
        }

        public void AddAgreement(UserAgreement ua)
        {
            //I really dont like this but there is not primary key on this table vv...
            var maxid = _inowdbContext.Camp_Data.Max(t => t.id);
            //yeah probably could have a dirty record ^^
            string[] values = ua.programLocation.Split('-');
            var location = values[0];
            var code = values[1];
            var campDat = new Camp_Data
            {
                //id = maxid + 1,
                campcode = code,
                sitename = location,
                begindate = Convert.ToDateTime(ua.programStartDate),
                enddate = Convert.ToDateTime(ua.programEndDate),
                returnstaff = ua.previouslyServed? "Yes":"No",
                firstname = ua.firstName,
                lastname = ua.lastName,
                address1 = ua.homeAddress,
                address2 = ua.homeAddressAddon,
                city = ua.userCity,
                state = ua.userState,
                zip = ua.userZip,
                dayphone = ua.cell,
                altphone = ua.altPhone,
                email = ua.email,
                socnum = ua.soc.ToString(),
                dob = ua.dateofBirth,
                position = ua.position,
                signedagreement = "yes",
                timestamp = DateTime.Now,   
                GUID = Guid.NewGuid(),
                dir_approval = null,
                notes = "",
                payapproval = "no",
                reason = "",
            };
            _inowdbContext.Add(campDat);
            _inowdbContext.SaveChanges();
        }

        public int GetProgramTypeId(string name)
        {
            if (name == "Camp Invention")
            {
                return 1;
            }
            else if (name == "Invention Project")
            {
                return 2;
            }
            else if (name == "Club Invention")
            {
                return 3;
            }
            else
            {
                return 99;
            }

        }
    }
}