using System.Collections.Generic;
using System.Threading.Tasks;
using Agreement.API.Models;
using System.Linq;

namespace Agreement.API.Persistence.Repositories.Interface
{
    public interface IcampDataRepo
    {
        Task<List<string>> GetStatesAsync(string position);

        Task<List<string>> GetCityAsync(string usstate, string position);

        Task<List<Camp_Data>> GetLocationAsync(string usstate, string city, string position);

        Task<List<string>> GetPositionAsync(string campCode);

        Task<List<Camp_Data_SiteCode>> GetDateRangeAsync(string campCode);
        List<Stipend> GetStipend(string position); //position in inowdb is role in cir fyi
        List<Stipend> GetStipend();

        List<string> GetVerbiage(string firstName, string lastName, string programLocation, string programCity, 
        string programState, string programStartDate, string programEndDate, string amtPaid, bool previouslyServed,
        string position, string PricingTypeName, bool isLegal, bool isStipendInfo, bool isVol, bool isThird, bool isDir);

        void AddAgreement(UserAgreement ua);
    }
}