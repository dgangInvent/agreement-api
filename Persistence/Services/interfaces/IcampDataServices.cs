using System.Threading.Tasks;
using Agreement.API.Models;
using System.Collections.Generic;

namespace Agreement.API.Services.Interface
{
    public interface IcampDataService
    {
        Task<string> GetStatesAsync(string position);

        Task<string> GetCityAsync(string usstate, string position);

        Task<string> GetLocationAsync(string usstate, string city, string position);

        Task<string> GetPositionAsync(string campCode);

        Task<string> GetDateRangeAsync(string campCode);
        string GetStipend(string position);
        string GetStipend();
        string GetVerbiage(string firstName, string lastName, string programLocation, string programCity, 
        string programState, string programStartDate, string programEndDate, string amtPaid, bool previouslyServed,
        string position, string PricingTypeName, bool isLegal, bool isStipendInfo, bool isVol, bool isThird, bool isDir);

        void PostAgreement(UserAgreement ua);
    }
}