using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Agreement.API.Models;
using Agreement.API.Persistence.Repositories;
using Agreement.API.Services.Interface;
using Agreement.API.Persistence.Repositories.Interface;
using System.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Agreement.API.Services
{
    public class CampDataService : IcampDataService
    {
        private readonly IcampDataRepo _campDataRepo;

        public CampDataService(IcampDataRepo campDataRepo)
        {
            _campDataRepo = campDataRepo;
        }

        public async Task<string> GetStatesAsync(string position)
        {
            var states = await _campDataRepo.GetStatesAsync(position);
            var usStates = new UsStates();
            usStates.value = new List<string>();
            foreach (var s in states)
            {
                usStates.value.Add(s);
            }
            string json = JsonConvert.SerializeObject(usStates, Formatting.None);
            return json;
        }

        public async Task<string> GetCityAsync(string usstate, string position)
        {
            var city = await _campDataRepo.GetCityAsync(usstate, position);
            var citys = new Citys();
            citys.value = new List<string>();
            foreach (var c in city)
            {
                citys.value.Add(c);
            }
            string json = JsonConvert.SerializeObject(citys, Formatting.None);
            return json;
        }

        public async Task<string> GetLocationAsync(string usstate, string city, string position)
        {
            var loca = await _campDataRepo.GetLocationAsync(usstate, city, position);
            var locas = new Locations();
            locas.value = new List<string>();
            foreach (var l in loca)
            {
                locas.value.Add(l.sitename + "-" + l.campcode);
            }
            string json = JsonConvert.SerializeObject(locas, Formatting.None);
            return json;
        }

        public async Task<string> GetPositionAsync(string campCode)
        {
            var position = await _campDataRepo.GetPositionAsync(campCode);
            var positions = new PositionAvailable();
            positions.value = new List<string>();
            foreach (var p in position)
            {
                positions.value.Add(p);
            }
            string json = JsonConvert.SerializeObject(positions, Formatting.None);
            return json;
        }

        public async Task<string> GetDateRangeAsync(string campCode)
        {
            var daterpo = await _campDataRepo.GetDateRangeAsync(campCode);
            var dates = new DateRange();
            dates.value = new List<string>();

            dates.value.Add(daterpo.First().StartDate.ToString());
            dates.value.Add(daterpo.First().EndDate.ToString());

            string json = JsonConvert.SerializeObject(dates, Formatting.None);
            return json;
        }

        public string GetStipend(string position)
        {
            var stipend = _campDataRepo.GetStipend(position);
            var stipends = new List<Stipend>();
            foreach (var s in stipend)
            {
                stipends.Add(s);
            }
            string json = JsonConvert.SerializeObject(stipends, Formatting.None);
            return json;
        }

        public string GetStipend()
        {
            var stipend = _campDataRepo.GetStipend();
            var stipends = new List<Stipend>();
            foreach (var s in stipend)
            {
                stipends.Add(s);
            }
            string json = JsonConvert.SerializeObject(stipends, Formatting.None);
            return json;
        }

        public string GetVerbiage(string firstName, string lastName, string programLocation, string programCity,
       string programState, string programStartDate, string programEndDate, string amtPaid, bool previouslyServed,
       string position, string PricingTypeName, bool isLegal, bool isStipendInfo, bool isVol, bool isThird, bool isDir)
        {
            var verb = _campDataRepo.GetVerbiage(firstName, lastName, programLocation, programCity,
                programState, programStartDate, programEndDate, amtPaid, previouslyServed,
                position, PricingTypeName, isLegal, isStipendInfo, isVol, isThird, isDir);
            string json = JsonConvert.SerializeObject(verb, Formatting.None);
            return json;
        }

        public void PostAgreement(UserAgreement ua)
        {
            _campDataRepo.AddAgreement(ua);
        }
    }
}